package io.gitlab.croclabs.cdb.helper;

import io.gitlab.croclabs.cdb.CrocConnection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SuppressWarnings("nullness")
public abstract class DatasourceTest {

	@Mock
	protected DataSource dataSource;
	@Mock
	protected Connection connection;
	@Mock
	protected PreparedStatement preparedStatement;
	@Mock
	protected ResultSet resultSet;

	@BeforeEach
	void beforeEach() throws SQLException {
		CrocConnection.setDataSource(dataSource);
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
	}
}
