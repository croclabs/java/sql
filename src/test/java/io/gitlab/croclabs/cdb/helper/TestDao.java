package io.gitlab.croclabs.cdb.helper;

import io.gitlab.croclabs.cdb.SqlQuery;

import java.util.List;

public interface TestDao {
	@SqlQuery("")
	List<String> test();
}
