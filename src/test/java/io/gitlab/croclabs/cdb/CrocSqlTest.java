package io.gitlab.croclabs.cdb;

import io.gitlab.croclabs.cdb.testing.CrocSqlExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(CrocSqlExtension.class)
@SuppressWarnings("nullness")
class CrocSqlTest {

	@Test
	void connect() {
		CrocSql.connect(cc -> {
			cc.update("CREATE TABLE test ( val1 VARCHAR(255) ) ");
			assertEquals(1, cc.update("INSERT INTO test VALUES('test') "));
			assertEquals(1, cc.update("UPDATE test SET val1 = 'test value'"));
			assertEquals("test value", cc.query(
					"SELECT * FROM test",
					rs -> rs.getString("val1")
			));

			cc.update("CREATE TABLE test2 ( val1 VARCHAR(255), val2 VARCHAR(255) ) ");
			assertEquals(1, cc.update(
					"INSERT INTO test2 VALUES(<?>)",
					List.of("test1", "test2")
			));
			String[] arr = cc.query(
					"SELECT * FROM test2",
					rs -> new String[] {
							rs.getString("val1"),
							rs.getString("val2")
					}
			);
			assertNotNull(arr);
			assertEquals("test1", arr[0]);
			assertEquals("test2", arr[1]);

			cc.update("CREATE TABLE test3 ( val1 VARCHAR(255), val2 VARCHAR(255), val3 VARCHAR(255) ) ");
			assertEquals(1, cc.update(
					"INSERT INTO test3 VALUES(?1, ?2, ?1)",
					"test1", "test2"
			));
			arr = cc.query(
					"SELECT * FROM test3",
					rs -> new String[] {
							rs.getString("val1"),
							rs.getString("val2"),
							rs.getString("val3")
					}
			);
			assertNotNull(arr);
			assertEquals("test1", arr[0]);
			assertEquals("test2", arr[1]);
			assertEquals("test1", arr[2]);
		});
	}
}