package io.gitlab.croclabs.cdb;

import org.checkerframework.checker.nullness.qual.Nullable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.regex.Pattern;

public class BeanResultHandler implements ResultHandler<Object> {
	private final Class<?> clazz;

	public BeanResultHandler(Class<?> clazz) {
		this.clazz = clazz;
	}

	public BeanResultHandler(Type type) {
		if (type.getTypeName().contains("java.util.List") ||
				type.getTypeName().contains("java.util.Set")) {
			String clazzStr = Pattern.compile("<.+>")
					.matcher(type.getTypeName())
					.results()
					.map(matchResult -> {
						String g = matchResult.group(0);
						return g.substring(1, g.length() - 1);
					})
					.findFirst()
					.orElse(null);

			if (clazzStr == null) {
				throw new CrocConnectionException("Class not extractable from List or Set");
			}

			try {
				this.clazz = Class.forName(clazzStr);
			} catch (ClassNotFoundException e) {
				throw new CrocConnectionException(e);
			}
		} else {
			try {
				this.clazz = Class.forName(type.getTypeName());
			} catch (ClassNotFoundException e) {
				throw new CrocConnectionException(e);
			}
		}
	}

	@Override
	public @Nullable Object handle(ResultSet rs) throws SQLException {
		if (clazz.equals(Integer.class)) {
			return rs.getInt(1);
		} else if (clazz.equals(Long.class)) {
			return rs.getLong(1);
		} else if (clazz.equals(String.class)) {
			return rs.getString(1);
		}

		try {
			Object instance = clazz.getDeclaredConstructor().newInstance();

			Arrays.stream(clazz.getDeclaredFields())
					.forEach(field -> fillField(rs, field, instance));

			return instance;
		} catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			throw new CrocConnectionException("BeanResultHandler could not create instance by default constructor.", e);
		}
	}

	@SuppressWarnings("nullness")
	private static void fillField(ResultSet rs, Field field, Object instance) {
		field.setAccessible(true);
		String name = field.getName();

		if (field.isAnnotationPresent(Column.class)) {
			name = field.getAnnotation(Column.class).value();
		}

		try {
			field.set(instance, rs.getObject(name));
		} catch (IllegalAccessException | SQLException e) {
			throw new CrocConnectionException("BeanResultHandler could not fill field.", e);
		}
	}
}
