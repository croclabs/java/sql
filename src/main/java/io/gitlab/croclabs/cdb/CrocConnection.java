package io.gitlab.croclabs.cdb;

import lombok.Getter;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import javax.sql.DataSource;
import java.sql.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Pattern;

@Getter
public final class CrocConnection implements AutoCloseable {
	private static final ThreadLocal<@Nullable CrocConnection> CON = new ThreadLocal<>();
	private static @Nullable DataSource dataSource = null;

	private final @Nullable Connection con;
	private final boolean autoCommit;

	private @Nullable Savepoint savepoint = null;
	private boolean committed;

	public CrocConnection() {
		this(true);
	}

	@SuppressWarnings("nullness")
	public CrocConnection(boolean autoCommit) {
		this.autoCommit = autoCommit;
		this.committed = autoCommit;

		if (dataSource != null) {
			try {
				this.con = dataSource.getConnection();
				con.setAutoCommit(autoCommit);
			} catch (SQLException e) {
				throw new CrocConnectionException(e);
			}
		} else {
			throw new CrocConnectionException("No datasource provided");
		}

		CON.set(this);
	}

	public void rollback() {
		Savepoint sp = this.savepoint;

		try {
			if (sp != null) {
				this.getCon().rollback(sp);
			} else {
				this.getCon().rollback();
			}
		} catch (SQLException e) {
			throw new CrocConnectionException(e);
		}
	}

	@Override
	public void close() {
		if (!committed) {
			this.rollback();
		}

		try {
			this.getCon().setAutoCommit(true);
			this.getCon().close();
		} catch (SQLException e) {
			throw new CrocConnectionException(e);
		}

		CON.remove();
	}

	public void commit() {
		try {
			this.getCon().commit();
		} catch (SQLException e) {
			throw new CrocConnectionException(e);
		}

		committed = true;
	}

	public void savepoint() {
		try {
			this.savepoint = this.getCon().setSavepoint();
		} catch (SQLException e) {
			throw new CrocConnectionException(e);
		}
	}

	public @Nullable <T> T query(
			@NonNull String sql,
			@NonNull ResultHandler<T> handler,
			@NonNull Object... args
	) {
		Entry<String, Object[]> en = createSqlNumbered(sql, args);

		try (@NonNull PreparedStatement ps = getCon().prepareStatement(createSql(en.getKey(), en.getValue()))) {
			prepare(ps, en.getValue());

			try (@NonNull ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					return handler.handle(rs);
				} else {
					return null;
				}
			}
		} catch (SQLException e) {
			throw new CrocConnectionException(e);
		}
	}

	public @NonNull <T> List<@Nullable T> queryList(
			@NonNull String sql,
			@NonNull ResultHandler<T> handler,
			@NonNull Object... args
	) {
		return query(sql, handler, new ArrayList<@Nullable T>(), args);
	}

	public @NonNull <T> Set<@Nullable T> querySet(
			@NonNull String sql,
			@NonNull ResultHandler<T> handler,
			@NonNull Object... args
	) {
		return query(sql, handler, new LinkedHashSet<@Nullable T>(), args);
	}

	private @NonNull <T, R extends Collection<@Nullable T>> R query(
			@NonNull String sql,
			@NonNull ResultHandler<T> handler,
			@NonNull R collection,
			@NonNull Object... args
	) {
		Entry<String, Object[]> en = createSqlNumbered(sql, args);

		try (@NonNull PreparedStatement ps = getCon().prepareStatement(createSql(en.getKey(), en.getValue()))) {
			prepare(ps, en.getValue());

			try (@NonNull ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					collection.add(handler.handle(rs));
				}

				return collection;
			}
		} catch (SQLException e) {
			throw new CrocConnectionException(e);
		}
	}

	public int update(@NonNull String sql, @NonNull Object... args) {
		Entry<String, Object[]> en = createSqlNumbered(sql, args);

		try (@NonNull PreparedStatement ps = getCon().prepareStatement(createSql(en.getKey(), en.getValue()))) {
			prepare(ps, en.getValue());
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new CrocConnectionException(e);
		}
	}

	private void prepare(@NonNull PreparedStatement ps, @NonNull Object... args) throws SQLException {
		if (args == null) {
			return;
		}

		int i = 0;

		for (Object arg : args) {
			if (arg instanceof Collection<?>) {
				for (Object subArg : (Collection<?>) arg) {
					ps.setObject(++i, subArg);
				}
			} else {
				ps.setObject(++i, arg);
			}
		}
	}

	private @NonNull Entry<String, Object[]> createSqlNumbered(String sql, Object... args) {
		String newSql = sql;
		List<Object> newArgs = new ArrayList<>();

		Pattern patternNumbered = Pattern.compile("\\?\\d+");
		List<String> groups = patternNumbered.matcher(sql)
				.results()
				.map(mr -> mr.group(0))
				.toList();

		if (groups.isEmpty()) {
			return new SimpleEntry<>(newSql, args);
		}

		try {
			groups.forEach(s -> {
				String s2 = s.replaceFirst("\\?", "");
				newArgs.add(args[Integer.parseInt(s2) - 1]);
			});
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new CrocConnectionException("Numbered parameters are greater than provided args", e);
		}

		newSql = newSql.replaceAll("\\?\\d+", "?");

		return new SimpleEntry<>(newSql, newArgs.toArray());
	}

	private @NonNull String createSql(String sql, Object... args) {
		String newSql = sql;

		Pattern pattern = Pattern.compile("<\\?>");
		long count = pattern.matcher(sql)
				.results()
				.map(mr -> mr.group(0))
				.count();

		if (count == 0) {
			return newSql;
		}

		for (Object arg : args) {
			if (arg instanceof Collection<?>) {
				StringJoiner sj = new StringJoiner(", ");
				((Collection<?>) arg).forEach(o -> sj.add("?"));
				newSql = newSql.replaceFirst("<\\?>", sj.toString());
				count--;
			}

			if (count == 0) {
				break;
			}
		}

		return newSql;
	}

	public static void setDataSource(@NonNull DataSource dataSource) {
		if (CrocConnection.dataSource == null) {
			CrocConnection.dataSource = dataSource;
		}
	}

	public static @NonNull CrocConnection get() {
		CrocConnection con = CON.get();

		if (con != null) {
			return con;
		} else {
			throw new CrocConnectionException("CrocConnection not present on thread");
		}
	}

	public @NonNull Connection getCon() {
		if (con == null) {
			throw new CrocConnectionException("No connection to get");
		}

		return con;
	}

	public static @NonNull DataSource getDataSource() {
		if (dataSource == null) {
			throw new CrocConnectionException("No datasource to get");
		}

		return dataSource;
	}

	public static boolean hasDataSource() {
		return dataSource != null;
	}
}
