package io.gitlab.croclabs.cdb;

import org.checkerframework.checker.nullness.qual.Nullable;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface ResultHandler<T> {
	@Nullable T handle(ResultSet rs) throws SQLException;
}
