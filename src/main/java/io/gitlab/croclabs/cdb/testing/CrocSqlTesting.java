package io.gitlab.croclabs.cdb.testing;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface CrocSqlTesting {
	String dbPath() default "mem:testDB";
	Parameter[] parameters() default {
			@Parameter(name = "DATABASE_TO_LOWER", value = "TRUE"),
			@Parameter(name = "CASE_INSENSITIVE_IDENTIFIERS", value = "TRUE")
	};
}
