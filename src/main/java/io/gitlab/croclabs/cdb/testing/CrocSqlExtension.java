package io.gitlab.croclabs.cdb.testing;

import io.gitlab.croclabs.cdb.CrocConnection;
import io.gitlab.croclabs.cdb.CrocSql;
import org.h2.jdbcx.JdbcDataSource;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import javax.naming.Context;
import javax.naming.InitialContext;
import java.util.Arrays;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicReference;

@CrocSqlTesting
public class CrocSqlExtension implements BeforeAllCallback, AfterAllCallback {
	@Override
	public void afterAll(ExtensionContext context) {
		CrocSql.connect(cc -> {
			cc.update("DROP ALL OBJECTS");
		});
	}

	@Override
	@SuppressWarnings("nullness")
	public void beforeAll(ExtensionContext context) throws Exception {
		if (CrocConnection.hasDataSource()) {
			return;
		}

		JdbcDataSource dataSource = new JdbcDataSource();

		System.setProperty(InitialContext.INITIAL_CONTEXT_FACTORY, "org.apache.naming.java.javaURLContextFactory");
		System.setProperty(InitialContext.URL_PKG_PREFIXES, "org.apache.naming");

		AtomicReference<CrocSqlTesting> cst = new AtomicReference<>(this.getClass().getAnnotation(CrocSqlTesting.class));
		context.getTestClass().ifPresent(c -> {
					if (c.isAnnotationPresent(CrocSqlTesting.class)) {
						cst.set(c.getAnnotation(CrocSqlTesting.class));
					}
		});

		StringJoiner url = new StringJoiner(";", "jdbc:h2:", "");
		url.add(cst.get().dbPath());
		Arrays.stream(cst.get().parameters()).forEach(p -> url.add(p.name() + "=" + p.value()));

		dataSource.setURL(url.toString());
		dataSource.setUser("sa");
		dataSource.setPassword("sa");
		Context ctx = new InitialContext();

		ctx.createSubcontext("java:");
		ctx.createSubcontext("java:comp");
		ctx.createSubcontext("java:comp/env");
		ctx.createSubcontext("java:comp/env/jdbc");
		ctx.bind("java:comp/env/jdbc/testDS", dataSource);

		CrocConnection.setDataSource(dataSource);
	}
}
