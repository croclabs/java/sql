package io.gitlab.croclabs.cdb;

public class CrocConnectionException extends RuntimeException {
	public CrocConnectionException() {
	}

	public CrocConnectionException(String message) {
		super(message);
	}

	public CrocConnectionException(String message, Throwable cause) {
		super(message, cause);
	}

	public CrocConnectionException(Throwable cause) {
		super(cause);
	}

	public CrocConnectionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
