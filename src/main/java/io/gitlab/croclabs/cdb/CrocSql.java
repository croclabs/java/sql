package io.gitlab.croclabs.cdb;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public final class CrocSql {

	private CrocSql() {
		super();
	}

	public static void transaction(@NonNull Runnable run) {
		doAction(crocConnection -> { run.run(); }, false);
	}

	public static void transaction(@NonNull Consumer<CrocConnection> consumer) {
		doAction(consumer, false);
	}

	public static @Nullable <T> T transaction(@NonNull Supplier<@Nullable T> supplier) {
		return doAction(crocConnection -> { return supplier.get(); }, false);
	}

	public static @Nullable <R> R transaction(@NonNull Function<CrocConnection, @Nullable R> fct) {
		return doAction(fct, false);
	}

	public static void connect(@NonNull Runnable run) {
		doAction(crocConnection -> { run.run(); }, true);
	}

	public static void connect(@NonNull Consumer<CrocConnection> consumer) {
		doAction(consumer, true);
	}

	public static @Nullable <T> T connect(@NonNull Supplier<@Nullable T> supplier) {
		return doAction(crocConnection -> { return supplier.get(); }, true);
	}

	public static @Nullable <R> R connect(@NonNull Function<CrocConnection, @Nullable R> fct) {
		return doAction(fct, true);
	}

	private static void doAction(@NonNull Consumer<CrocConnection> consumer, boolean autoCommit) {
		try (CrocConnection con = new CrocConnection(autoCommit)) {
			consumer.accept(con);
			con.commit();
		}
	}

	private static @Nullable <R> R doAction(@NonNull Function<CrocConnection, @Nullable R> function, boolean autoCommit) {
		try (CrocConnection con = new CrocConnection(autoCommit)) {
			R obj = function.apply(con);
			con.commit();
			return obj;
		}
	}

	@SuppressWarnings({"unchecked", "nullness"})
	public static @NonNull <T> T transactional(@NonNull Class<T> interfaceClass) {
		return (T) Proxy.newProxyInstance(
				CrocConnection.class.getClassLoader(),
				new Class[]{ interfaceClass },
				new TransactionInvocationHandler()
		);
	}

	@SuppressWarnings({"unchecked", "nullness"})
	public static @NonNull <T> T dao(@NonNull Class<T> interfaceClass) {
		return (T) Proxy.newProxyInstance(
				CrocConnection.class.getClassLoader(),
				new Class[]{ interfaceClass },
				new DaoInvocationHandler()
		);
	}

	final static class TransactionInvocationHandler implements InvocationHandler {

		@Override
		@SuppressWarnings("nullness")
		public @Nullable Object invoke(Object proxy, @NonNull Method method, Object[] args) throws Throwable {
			if (isTransaction(method)) {
				return invokeTransaction(proxy, method, args);
			} else if (method.isDefault()) {
				return InvocationHandler.invokeDefault(proxy, method, args);
			}

			return null;
		}
	}

	final static class DaoInvocationHandler implements InvocationHandler {

		@Override
		@SuppressWarnings("nullness")
		public @Nullable Object invoke(Object proxy, @NonNull Method method, Object[] args) throws Throwable {
			if (isTransaction(method)) {
				return invokeTransaction(proxy, method, args);
			} else if (method.isDefault()) {
				return InvocationHandler.invokeDefault(proxy, method, args);
			} else if (method.isAnnotationPresent(SqlQuery.class)) {
				return invokeQuery(method, args);
			} else if (method.isAnnotationPresent(SqlUpdate.class)) {
				return invokeUpdate(method, args);
			}

			return null;
		}
	}

	@SuppressWarnings("nullness")
	private static @NonNull Object invokeUpdate(@NonNull Method method, Object[] args) {
		SqlUpdate sqlUpdate = method.getAnnotation(SqlUpdate.class);
		String query = sqlUpdate.value();

		if (method.getReturnType().equals(Integer.class)) {
			return CrocConnection.get().update(query, args);
		} else if (method.getReturnType().equals(Boolean.class)) {
			return (CrocConnection.get().update(query, args) > 0);
		} else {
			throw new CrocConnectionException("Unsupported return type for @SqlUpdate." +
					" Only boolean and int are supported.");
		}
	}

	@SuppressWarnings("nullness")
	private static @Nullable Object invokeQuery(@NonNull Method method, Object[] args) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		SqlQuery sqlQuery = method.getAnnotation(SqlQuery.class);
		String query = sqlQuery.value();
		ResultHandler<?> handler = null;

		if (method.isAnnotationPresent(Result.class)) {
			handler = method.getAnnotation(Result.class).handler().getDeclaredConstructor().newInstance();
		}

		if (method.getReturnType().equals(List.class)) {
			handler = Objects.requireNonNullElse(handler, new BeanResultHandler(method.getGenericReturnType()));
			return CrocConnection.get().queryList(query, handler, args);
		} else if (method.getReturnType().equals(Set.class)) {
			handler = Objects.requireNonNullElse(handler, new BeanResultHandler(method.getGenericReturnType()));
			return CrocConnection.get().querySet(query, handler, args);
		}

		handler = Objects.requireNonNullElse(handler, new BeanResultHandler(method.getReturnType()));
		return CrocConnection.get().query(query, handler, args);
	}

	private static @Nullable Object invokeTransaction(Object proxy, @NonNull Method method, Object[] args) {
		return transaction(() -> {
			try {
				return InvocationHandler.invokeDefault(proxy, method, args);
			} catch (Throwable e) {
				throw new CrocConnectionException(e);
			}
		});
	}

	private static boolean isTransaction(@NonNull Method method) {
		return (method.isAnnotationPresent(Transaction.class) ||
				method.getDeclaringClass().isAnnotationPresent(Transaction.class))
				&& method.isDefault();
	}
}
