package io.gitlab.croclabs.cql;

import java.sql.SQLException;

@FunctionalInterface
public interface TransactionRunner {
	void run() throws SQLException;
}
