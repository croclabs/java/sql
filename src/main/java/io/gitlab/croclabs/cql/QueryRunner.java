package io.gitlab.croclabs.cql;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface QueryRunner {
	void run(ResultSet rs) throws SQLException;
}
