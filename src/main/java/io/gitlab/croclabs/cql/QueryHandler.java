package io.gitlab.croclabs.cql;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface QueryHandler<T> {
	T handle(ResultSet rs) throws SQLException;
}
