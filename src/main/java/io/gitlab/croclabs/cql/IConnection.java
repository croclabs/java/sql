package io.gitlab.croclabs.cql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Savepoint;

public interface IConnection extends AutoCloseable {
	Savepoint savepoint() throws SQLException;
	Savepoint savepoint(String name) throws SQLException;

	/**
	 * Completely rolls back the whole transaction
	 *
	 * @throws SQLException if rollback failed
	 */
	void rollback() throws SQLException;

	/**
	 * Rolls back to the specified savepoint. If the savepoint
	 * is not found, does not perform a rollback.
	 *
	 * @param name name of the savepoint to roll back to
	 * @throws SQLException if rollback failed
	 */
	void rollback(String name) throws SQLException;

	Connection getCon();
	Savepoint getSavepoint(String name);

	/**
	 * Method to prepare the statement
	 *
	 * @param ps the statement to prepare
	 * @param params the params to prepare the statement with
	 * @throws SQLException if preparing goes wrong
	 */
	void prepare(PreparedStatement ps, Object... params) throws SQLException;

	/**
	 * Use to pre-process the SQL to add functionality like named queries.
	 *
	 * @param sql the sql to process
	 * @param params the params to process
	 * @return instance holding the processed sql and params
	 */
	SqlHolder processSql(String sql, Object... params);

	/**
	 * Closes, commits and resets the associated {@link Connection} and cleans the
	 * {@link ThreadLocal} instance of the {@link IConnection}
	 * object.
	 *
	 * @throws SQLException if committing, setting options or closing went wrong
	 */
	@Override
	default void close() throws Exception {
		CQL.CC.remove();

		if (getCon() == null) {
			return;
		}

		getCon().commit();
		getCon().setAutoCommit(true);
		getCon().close();
	}
}
