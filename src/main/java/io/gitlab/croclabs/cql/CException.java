package io.gitlab.croclabs.cql;

public class CException extends RuntimeException {
	public CException() {
		super();
	}

	public CException(String message) {
		super(message);
	}

	public CException(String message, Throwable cause) {
		super(message, cause);
	}

	public CException(Throwable cause) {
		super(cause);
	}

	public CException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
