package io.gitlab.croclabs.cql;

import java.sql.SQLException;

@FunctionalInterface
public interface ConnectionSupplier<T extends IConnection> {
	T supply() throws SQLException;
}
