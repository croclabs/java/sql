package io.gitlab.croclabs.cql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.HashMap;
import java.util.Map;

public class CConnection implements IConnection {
	private final Connection con;
	private final Map<String, Savepoint> savepoints = new HashMap<>();

	protected CConnection() throws SQLException {
		super();

		con = CQL.dataSource.getConnection();
		con.setAutoCommit(false);
	}

	@Override
	public Savepoint savepoint() throws SQLException {
		Savepoint savepoint = con.setSavepoint();
		savepoints.put(savepoint.getSavepointName(), savepoint);
		return savepoint;
	}

	@Override
	public Savepoint savepoint(String name) throws SQLException {
		Savepoint savepoint = con.setSavepoint(name);
		savepoints.put(savepoint.getSavepointName(), savepoint);
		return savepoint;
	}

	@Override
	public void rollback() throws SQLException {
		con.rollback();
	}

	@Override
	public void rollback(String name) throws SQLException {
		if (!savepoints.isEmpty()) {
			con.rollback(savepoints.get(name));
		} else {
			con.rollback();
		}
	}

	@Override
	public void prepare(PreparedStatement ps, Object... params) throws SQLException {
		int i = 1;
		for (Object param : params) {
			ps.setObject(i, param);
			i++;
		}
	}

	@Override
	public SqlHolder processSql(String sql, Object... params) {
		return new SqlHolder(
				sql,
				params
		);
	}

	@Override
	public Connection getCon() {
		return con;
	}

	@Override
	public Savepoint getSavepoint(String name) {
		return savepoints.get(name);
	}
}
