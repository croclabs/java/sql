package io.gitlab.croclabs.cql;

import java.sql.SQLException;

@FunctionalInterface
public interface TransactionHandler<T> {
	T handle() throws SQLException;
}
