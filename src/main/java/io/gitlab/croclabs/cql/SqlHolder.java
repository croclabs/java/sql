package io.gitlab.croclabs.cql;

public class SqlHolder {
	protected final String sql;
	protected final Object[] params;

	public SqlHolder(String sql, Object[] params) {
		this.sql = sql;
		this.params = params;
	}
}
