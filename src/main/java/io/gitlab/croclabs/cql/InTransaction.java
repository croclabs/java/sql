package io.gitlab.croclabs.cql;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Use to mark a method for use inside active transactions ONLY
 */
@Documented
@Target(ElementType.METHOD)
public @interface InTransaction {
}
