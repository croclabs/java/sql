package io.gitlab.croclabs.cql;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CQL {
	protected static DataSource dataSource;
	protected static ConnectionSupplier<?> supplier = CConnection::new;
	protected static final ThreadLocal<IConnection> CC = new ThreadLocal<>();

	public static void supply(ConnectionSupplier<?> supplier) {
		CQL.supplier = supplier;
	}

	public static void savepoint() throws SQLException {
		if (CC.get() != null) {
			CC.get().savepoint();
		}
	}

	public static void savepoint(String name) throws SQLException {
		if (CC.get() != null) {
			CC.get().savepoint(name);
		}
	}

	/**
	 * Completely rolls back the whole transaction
	 *
	 * @throws SQLException if rollback failed
	 */
	public static void rollback() throws SQLException {
		if (CC.get() != null) {
			CC.get().rollback();
		}
	}

	/**
	 * Rolls back to the specified savepoint. If the savepoint
	 * is not found, does not perform a rollback.
	 *
	 * @param name name of the savepoint to roll back to
	 * @throws SQLException if rollback failed
	 */
	public static void rollback(String name) throws SQLException {
		if (CC.get() != null && CC.get().getSavepoint(name) != null) {
			CC.get().rollback(name);
		}
	}

	public static <T> T transaction(TransactionHandler<T> handler) {
		try (IConnection cc = supplier.supply()) {
			CC.set(cc);
			return handler.handle();
		} catch (Exception e) {
			throw new CException(e);
		}
	}

	public static void transaction(TransactionRunner handler) {
		try (IConnection cc = supplier.supply()) {
			CC.set(cc);
			handler.run();
		} catch (Exception e) {
			throw new CException(e);
		}
	}

	public static void queryAll(String sql, QueryRunner handler, Object... params) {
		IConnection cc = CC.get();

		if (cc != null) {
			try {
				queryAll(cc, sql, handler, params);
			} catch (SQLException e) {
				throw new CException(e);
			}
		} else {
			transaction(() -> queryAll(CC.get(), sql, handler, params));
		}
	}

	private static void queryAll(IConnection cc, String sql, QueryRunner handler, Object... params) throws SQLException {
		Connection con = cc.getCon();
		SqlHolder sqlHolder = cc.processSql(sql, params);

		try (PreparedStatement ps = con.prepareStatement(sqlHolder.sql)) {
			cc.prepare(ps, sqlHolder.params);

			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					handler.run(rs);
				}
			}
		}
	}

	public static <T> T query(String sql, QueryHandler<T> handler, Object... params) {
		IConnection cc = CC.get();

		if (cc != null) {
			try {
				return query(cc, sql, handler, params);
			} catch (SQLException e) {
				throw new CException(e);
			}
		} else {
			return transaction(() -> query(CC.get(), sql, handler, params));
		}
	}

	private static <T> T query(IConnection cc, String sql, QueryHandler<T> handler, Object... params) throws SQLException {
		Connection con = cc.getCon();
		SqlHolder sqlHolder = cc.processSql(sql, params);

		try (PreparedStatement ps = con.prepareStatement(sqlHolder.sql)) {
			cc.prepare(ps, sqlHolder.params);

			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					return handler.handle(rs);
				} else {
					return null;
				}
			}
		}
	}

	public static <T> List<T> queryList(String sql, QueryHandler<T> handler, Object... params) {
		IConnection cc = CC.get();

		if (cc != null) {
			try {
				return queryList(cc, sql, handler, params);
			} catch (SQLException e) {
				throw new CException(e);
			}
		} else {
			return transaction(() -> queryList(CC.get(), sql, handler, params));
		}
	}

	private static <T> List<T> queryList(IConnection cc, String sql, QueryHandler<T> handler, Object... params) throws SQLException {
		Connection con = cc.getCon();
		SqlHolder sqlHolder = cc.processSql(sql, params);

		try (PreparedStatement ps = con.prepareStatement(sqlHolder.sql)) {
			cc.prepare(ps, sqlHolder.params);

			try (ResultSet rs = ps.executeQuery()) {
				List<T> list = new ArrayList<>();

				while (rs.next()) {
					list.add(handler.handle(rs));
				}

				return list;
			}
		}
	}

	public static int update(String sql, Object... params) {
		IConnection cc = CC.get();

		if (cc != null) {
			try {
				return update(cc, sql, params);
			} catch (SQLException e) {
				throw new CException(e);
			}
		} else {
			return transaction(() -> update(CC.get(), sql, params));
		}
	}

	private static int update(IConnection cc, String sql, Object... params) throws SQLException {
		Connection con = cc.getCon();
		SqlHolder sqlHolder = cc.processSql(sql, params);

		try (PreparedStatement ps = con.prepareStatement(sqlHolder.sql)) {
			cc.prepare(ps, sqlHolder.params);
			return ps.executeUpdate();
		}
	}
}
